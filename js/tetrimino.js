function Tetrimino(iniX, iniY, rot, offsetData){
    this.position = {x:iniX, y:iniY};
    this.rotation = rot;
    var data = offsetData;

    this.getData = function(){
        return angular.copy(data[this.rotation]);
    };

    this.setData = function(offsetData){
        data = offsetData;
    };
};