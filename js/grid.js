function Grid(h, w){
    var height=h;
    var width=w;
    var grid = [];
    for (var i=0; i<height; ++i){
        var row = [];
        for (var j=0; j<width; ++j)
            row.push(false);
        grid.push(row);
    }

    this.getGrid = function(){ return grid; };

    // this.set = function(i,j,value){
    //     if ((i>=0 && i<height) && (j>=0 && j<width))
    //         grid[i][j] = value;
    // };

    this.get = function(i,j){
        if ((i>=0 && i<height) && (j>=0 && j<width))
            return grid[i][j];
        else
            return undefined;
    };

    this.erase = function(tetrimino){
        var offset = tetrimino.getData();
        var position = tetrimino.position;
        for (var i=0; i<offset.length; ++i){
            grid[position.y+offset[i].y][position.x+offset[i].x] = false;
        }
    };

    this.draw = function(tetrimino){
        var offset = tetrimino.getData();
        var position = tetrimino.position;
        for (var i=0; i<offset.length; ++i){
            grid[position.y+offset[i].y][position.x+offset[i].x] = true;
        }
    };

    this.checkLines = function(){
        var score = 0;
        for (var i=0; i<height; ++i)
            if (isLineFull(i)){
                score += 100;
                dropLines(i);
            }
        return score;
    };

    var isLineFull = function(i){
        for (var j=0; j<width; ++j)
            if (!grid[i][j])
                return false;
        return true;
    };

    var dropLines = function(i){
        for (var j=0; j<width; ++j)
            grid[i][j] = false;

        for (var p=i-1; p>=0; --p){
            for (var j=0; j<width; ++j)
                grid[p+1][j] = grid[p][j];
        }
    };
};