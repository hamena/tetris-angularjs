function buildOffsetL(){
    return [
        [
            {x:1, y:0},
            {x:1, y:1},
            {x:1, y:2},
            {x:2, y:2}
        ],[
            {x:2, y:0},
            {x:0, y:1},
            {x:1, y:1},
            {x:2, y:1}
        ],[
            {x:0, y:0},
            {x:1, y:0},
            {x:1, y:1},
            {x:1, y:2}
        ],[
            {x:0, y:1},
            {x:1, y:1},
            {x:2, y:1},
            {x:0, y:2}
        ]
    ];
};

function buildOffsetL2(){
    return [
        [
            {x:1, y:0},
            {x:1, y:1},
            {x:1, y:2},
            {x:0, y:2}
        ],[
            {x:2, y:2},
            {x:0, y:1},
            {x:1, y:1},
            {x:2, y:1}
        ],[
            {x:2, y:0},
            {x:1, y:0},
            {x:1, y:1},
            {x:1, y:2}
        ],[
            {x:0, y:1},
            {x:1, y:1},
            {x:2, y:1},
            {x:0, y:0}
        ]
    ];
};

function buildOffsetS(){
    return [
        [
            {x:0, y:0},
            {x:0, y:1},
            {x:1, y:1},
            {x:1, y:2}
        ],[
            {x:0, y:2},
            {x:1, y:1},
            {x:1, y:2},
            {x:2, y:1}
        ],[
            {x:0, y:0},
            {x:0, y:1},
            {x:1, y:1},
            {x:1, y:2}
        ],[
            {x:0, y:2},
            {x:1, y:1},
            {x:1, y:2},
            {x:2, y:1}
        ]
    ];
};

function buildOffsetZ(){
    return [
        [
            {x:2, y:0},
            {x:2, y:1},
            {x:1, y:1},
            {x:1, y:2}
        ],[
            {x:0, y:1},
            {x:1, y:1},
            {x:1, y:2},
            {x:2, y:2}
        ],[
            {x:2, y:0},
            {x:2, y:1},
            {x:1, y:1},
            {x:1, y:2}
        ],[
            {x:0, y:1},
            {x:1, y:1},
            {x:1, y:2},
            {x:2, y:2}
        ]
    ];
};

function buildOffsetT(){
    return [
        [
            {x:0, y:1},
            {x:1, y:1},
            {x:2, y:1},
            {x:1, y:0}
        ],[
            {x:1, y:0},
            {x:1, y:1},
            {x:1, y:2},
            {x:0, y:1}
        ],[
            {x:0, y:1},
            {x:1, y:1},
            {x:2, y:1},
            {x:1, y:2}
        ],[
            {x:1, y:0},
            {x:1, y:1},
            {x:1, y:2},
            {x:2, y:1}
        ]
    ];
};

function buildOffsetSquare(){
    return [
        [
            {x:0, y:0},
            {x:0, y:1},
            {x:1, y:0},
            {x:1, y:1}
        ],[
            {x:0, y:0},
            {x:0, y:1},
            {x:1, y:0},
            {x:1, y:1}
        ],[
            {x:0, y:0},
            {x:0, y:1},
            {x:1, y:0},
            {x:1, y:1}
        ],[
            {x:0, y:0},
            {x:0, y:1},
            {x:1, y:0},
            {x:1, y:1}
        ]
    ];
};

function buildOffsetLine(){
    return [
        [
            {x:1, y:0},
            {x:1, y:1},
            {x:1, y:2},
            {x:1, y:3}
        ],[
            {x:0, y:1},
            {x:1, y:1},
            {x:2, y:1},
            {x:3, y:1}
        ],[
            {x:1, y:0},
            {x:1, y:1},
            {x:1, y:2},
            {x:1, y:3}
        ],[
            {x:0, y:1},
            {x:1, y:1},
            {x:2, y:1},
            {x:3, y:1}
        ]
    ];
};

function buildRandomOffset(){
    var random = getRandomInt(0,7);
    switch(random){
        case 0:
            return buildOffsetL();
        case 1:
            return buildOffsetL2();
        case 2:
            return buildOffsetS();
        case 3:
            return buildOffsetZ();
        case 4:
            return buildOffsetT();
        case 5:
            return buildOffsetSquare();
        case 6:
            return buildOffsetLine();
    }
};