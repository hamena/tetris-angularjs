function Tetris() {
    var iniX = 4;
    var iniY = 0;
    var iniRotation = 0;
    var tetrimino = new Tetrimino(iniX, iniY, iniRotation, buildRandomOffset());
    var nextOffset = buildRandomOffset();
    var grid = new Grid(17, 11);
    var score = 0;
    this.gameOver = undefined;

    this.getGrid = function(){
        return grid.getGrid();
    };

    this.getScore = function(){
        return score;
    };

    this.rotate = function () {
        grid.erase(tetrimino);
        tetrimino.rotation = ++tetrimino.rotation % 4;
        if (collision())
            tetrimino.rotation = --tetrimino.rotation % 4;
        grid.draw(tetrimino);
    };

    this.moveLeft = function () {
        grid.erase(tetrimino);
        --tetrimino.position.x;
        if (collision())
            ++tetrimino.position.x;
            grid.draw(tetrimino);
    };

    this.moveRight = function () {
        grid.erase(tetrimino);
        ++tetrimino.position.x;
        if (collision())
            --tetrimino.position.x;
            grid.draw(tetrimino);
    };

    this.moveUp = function () {
        grid.erase(tetrimino);
        --tetrimino.position.y;
        if (collision())
            ++tetrimino.position.y;
            grid.draw(tetrimino);
    };

    this.moveDown = function () {
        grid.erase(tetrimino);
        ++tetrimino.position.y;
        if (collision()) {
            score += 10;
            --tetrimino.position.y;
            grid.draw(tetrimino);
            reset();
            score += grid.checkLines();
        }
        grid.draw(tetrimino);
    };

    this.drop = function () {
        grid.erase(tetrimino);
        do {
            ++tetrimino.position.y;
        } while (!collision());
        score += 10;
        --tetrimino.position.y;
        grid.draw(tetrimino);
        reset();
        score += grid.checkLines();
        grid.draw(tetrimino);
    };

    var collision = function () {
        var existsCollision = false;
        var offset = tetrimino.getData();
        for (var i = 0; !existsCollision && i < offset.length; ++i) {
            var value = grid.get(tetrimino.position.y + offset[i].y, tetrimino.position.x + offset[i].x);
            existsCollision = value == undefined || value;
        }
        return existsCollision;
    };

    var reset = function () {
        tetrimino.position.x = iniX;
        tetrimino.position.y = iniY;
        tetrimino.rotation = iniRotation;
        tetrimino.setData(nextOffset);
        nextOffset = buildRandomOffset();

        if (collision())
            gameOver = "Game Over";
    };
};