
function Player(iniX, iniY, gr){
    var position = {x:iniX, y:iniY};

    var grid = gr;
    grid.set(position.y, position.x, true);

    this.moveLeft = function(){
        var value = grid.get(position.y, position.x-1);
        if (value!=undefined && !value){
            grid.set(position.y, position.x, false);
            --position.x;
            grid.set(position.y, position.x, true);
        }
    };

    this.moveRight = function(){
        var value = grid.get(position.y, position.x+1);
        if (value!=undefined && !value){
            grid.set(position.y, position.x, false);
            ++position.x;
            grid.set(position.y, position.x, true);
        }
    };

    this.moveUp = function(){
        var value = grid.get(position.y-1, position.x);
        if (value!=undefined && !value){
            grid.set(position.y, position.x, false);
            --position.y;
            grid.set(position.y, position.x, true);
        }
    };

    this.moveDown = function(){
        var value = grid.get(position.y+1, position.x);
        if (value!=undefined && !value){
            grid.set(position.y, position.x, false);
            ++position.y;
            grid.set(position.y, position.x, true);
        }
    };

    this.getPosition = function(){
        return position;
    };
};